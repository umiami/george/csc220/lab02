[![pipeline status](https://gitlab.com/umiami/george/csc220/lab02/badges/master/pipeline.svg)](https://gitlab.com/umiami/george/csc220/lab02/-/pipelines?ref=master)
[![compile](https://gitlab.com/umiami/george/csc220/lab02/builds/artifacts/master/raw/.results/compile.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/lab02/-/jobs/artifacts/master/file/.results/compile.log?job=evaluate)
[![checkstyle](https://gitlab.com/umiami/george/csc220/lab02/builds/artifacts/master/raw/.results/checkstyle.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/lab02/-/jobs/artifacts/master/file/.results/checkstyle.log?job=evaluate)
[![test](https://gitlab.com/umiami/george/csc220/lab02/builds/artifacts/master/raw/.results/test.svg?job=evaluate)](https://gitlab.com/umiami/george/csc220/lab02/-/jobs/artifacts/master/file/.results/test.log?job=evaluate)

# Lab02: Matrix

> This assignment project represents 2D matrices with two different constructors and several basic operations (methods). Note: The `Matrix` class represents `Matrix` objects, therefore its methods are not static. In order to call these methods, you must call them from an object of the `Matrix` class.
