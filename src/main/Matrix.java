public class Matrix {
  int numRows;
  int numColumns;
  int[][] data;

  /**
   * Default constructor for empty matrix.
   */
  public Matrix() {
  }

  /**
   * Creates a matrix of given size and initialize its values to 0.
   *
   * @param rowDim number of rows
   * @param colDim number of columns
   */
  public Matrix(int rowDim, int colDim) {
    numRows = rowDim;
    numColumns = colDim;
    data = new int[numRows][numColumns];
  }

  /**
   * Creates matrix from given array. Automatically determines dimensions.
   * 1) Put the numRows to be the number of 1D arrays in the 2D array.
   * 2) Specify the numColumns and set it.
   * 3) Handle special cases properly!
   * 4) Create a new matrix to hold the data.
   * 5) Copy the data over.
   *
   * @param source array
   */
  public Matrix(int[][] source) {
    numRows = source.length;
    if (numRows == 0) {
      numColumns = 0;
    } else {
      numColumns = source[0].length;
    }
    data = new int[numRows][numColumns];
    for (int i = 0; i < numRows; i++) {
      for (int j = 0; j < numColumns; j++) {
        data[i][j] = source[i][j];
      }
    }
  }

  /**
   * Prints a string that represents this matrix.
   * - Columns separated by space.
   * - Rows separated by newlines.
   * - Last line separated with a newline.
   */
  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    for (int row = 0; row < numRows; row++) {
      for (int col = 0; col < numColumns; col++) {
        s.append(data[row][col]);
        if (col != numColumns - 1) {
          s.append(" ");
        }
      }
      s.append("\n");
    }
    return s.toString();
  }

  /**
   * Compares current Matrix with given obj.
   *
   * @param obj object to compare to
   * @return a boolean indicating whether the other Object represents the same Matrix as this one
   */
  @Override
  public boolean equals(Object obj) {
    // make sure the Object we're comparing to is a Matrix
    if (!(obj instanceof Matrix)) {
      return false;
    }
    // if the above was not true, we know it's safe to treat 'obj' as a Matrix
    Matrix m = (Matrix) obj;
    if (!(this.numRows == m.numRows && this.numColumns == m.numColumns)) {
      return false;
    }
    for (int row = 0; row < m.numRows; row++) {
      for (int col = 0; col < m.numColumns; col++) {
        if (data[row][col] != m.data[row][col]) {
          return false;
        }
      }
    }
    return true;
  }

  /**
   * Checks if the two matrices are compatible for multiplication.
   * Determine the dimensions of the resulting matrix.
   * Creates a new Matrix and fills it with the correct values for matrix multiplication.
   *
   * @param second the Matrix to be multiplied by (the right-hand side of a multiplication)
   * @return a new Matrix that is the result of this Matrix multiplied by the input Matrix or null
   */
  public Matrix times(Matrix second) {
    if (numColumns != second.numRows) {
      return null;
    }
    Matrix toReturn = new Matrix(numRows, second.numColumns);
    for (int row = 0; row < numRows; row++) {
      for (int col = 0; col < second.numColumns; col++) {
        for (int k = 0; k < numColumns; k++) {
          toReturn.data[row][col] += data[row][k] * second.data[k][col];
        }
      }
    }
    return toReturn;
  }

  /**
   * Checks if the two matrices are compatible for addition.
   * Determine the dimensions of the resulting matrix.
   * Creates a new Matrix and fills it with the correct values for matrix addition.
   *
   * @param second the Matrix to be added (right-hand side of addition)
   * @return a new Matrix that is the result of this Matrix added to the input Matrix or null
   */
  public Matrix plus(Matrix second) {
    if (!(this.numRows == second.numRows && this.numColumns == second.numColumns)) {
      return null;
    }
    Matrix toReturn = new Matrix(second.numRows, second.numColumns);
    for (int row = 0; row < second.numRows; row++) {
      for (int col = 0; col < second.numColumns; col++) {
        toReturn.data[row][col] = data[row][col] + second.data[row][col];
      }
    }
    return toReturn;
  }

  /**
   * Transpose the current Matrix.
   *
   * @return a new matrix that is the transpose of this matrix
   */
  public Matrix transpose() {
    int[][] temp = new int[numColumns][numRows];
    for (int i = 0; i < numColumns; i++) {
      for (int j = 0; j < numRows; j++) {
        temp[i][j] = data[j][i];
      }
    }
    return new Matrix(temp);
  }

  /**
   * Indicates that the program is running.
   * Runs several method on sample matrices.
   * Tests weather methods are implemented.
   * The output of this method does not really matter.
   * Use it for debugging purspose.
   */
  public static void main(String[] args) {
    System.out.println("Running main method...");
    Matrix m1 = new Matrix(new int[][] {{1, 2, 3}, {1, 2, 3}, {1, 2, 3}});
    Matrix m2 = new Matrix(new int[][] {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
    Matrix m3 = m1.plus(m2);
    Matrix m4 = m1.times(m2);
    if (m3 != null) {
      System.out.println(m3.toString());
    }
    if (m4 != null) {
      System.out.println(m4.toString());
    }
    System.out.println("Done.");
  }

}
